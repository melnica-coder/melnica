<?php

/**
 * This is the model class for table "floor".
 *
 * The followings are the available columns in table 'floor':
 * @property integer $id
 * @property integer $number
 * @property integer $free_space
 * @property integer $occupied_space
 * @property integer $total_space
 * @property string $background_filename
 * @property string $occupied_filename
 * @property integer $image_gallery_id
 * @property integer $image_height
 *
 * The followings are the available model relations:
 * @property ImageGallery $imageGallery
 * @property Resident[] $residents
 */
class Floor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'floor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, free_space, occupied_space, total_space, image_gallery_id', 'required'),
			array('number, free_space, occupied_space, total_space, image_gallery_id, image_height', 'numerical', 'integerOnly'=>true),
			array('background_filename, occupied_filename', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, free_space, occupied_space, total_space, background_filename, occupied_filename, image_gallery_id, image_height', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imageGallery' => array(self::BELONGS_TO, 'ImageGallery', 'image_gallery_id'),
			'residents' => array(self::HAS_MANY, 'Resident', 'floor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'number' => 'Number',
			'free_space' => 'Free Space',
			'occupied_space' => 'Occupied Space',
			'total_space' => 'Total Space',
			'background_filename' => 'Background Filename',
			'occupied_filename' => 'Occupied Filename',
			'image_gallery_id' => 'Image Gallery',
			'image_height' => 'Image Height',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number);
		$criteria->compare('free_space',$this->free_space);
		$criteria->compare('occupied_space',$this->occupied_space);
		$criteria->compare('total_space',$this->total_space);
		$criteria->compare('background_filename',$this->background_filename,true);
		$criteria->compare('occupied_filename',$this->occupied_filename,true);
		$criteria->compare('image_gallery_id',$this->image_gallery_id);
		$criteria->compare('image_height',$this->image_height);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Floor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
