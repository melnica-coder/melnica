<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property string $header
 * @property integer $type
 * @property string $date
 * @property string $link
 * @property string $text_1
 * @property string $text_2
 * @property integer $image_gallery_id
 * @property string $banner_filename
 * @property string $icon_filename
 * @property integer $is_shown
 * @property integer $is_important
 * @property string $vk_link_title
 * @property string $link_fb
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property ImageGallery $imageGallery
 */
class Event extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('header, type, date, text_1, text_2, image_gallery_id, is_shown, is_important, rating', 'required'),
			array('type, image_gallery_id, is_shown, is_important, rating', 'numerical', 'integerOnly'=>true),
			array('header, link, banner_filename, icon_filename, vk_link_title, link_fb', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, header, type, date, link, text_1, text_2, image_gallery_id, banner_filename, icon_filename, is_shown, is_important, vk_link_title, link_fb, rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imageGallery' => array(self::BELONGS_TO, 'ImageGallery', 'image_gallery_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'header' => 'Header',
			'type' => 'Type',
			'date' => 'Date',
			'link' => 'Link',
			'text_1' => 'Text 1',
			'text_2' => 'Text 2',
			'image_gallery_id' => 'Image Gallery',
			'banner_filename' => 'Banner Filename',
			'icon_filename' => 'Icon Filename',
			'is_shown' => 'Is Shown',
			'is_important' => 'Is Important',
			'vk_link_title' => 'Vk Link Title',
			'link_fb' => 'Link Fb',
			'rating' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('text_1',$this->text_1,true);
		$criteria->compare('text_2',$this->text_2,true);
		$criteria->compare('image_gallery_id',$this->image_gallery_id);
		$criteria->compare('banner_filename',$this->banner_filename,true);
		$criteria->compare('icon_filename',$this->icon_filename,true);
		$criteria->compare('is_shown',$this->is_shown);
		$criteria->compare('is_important',$this->is_important);
		$criteria->compare('vk_link_title',$this->vk_link_title,true);
		$criteria->compare('link_fb',$this->link_fb,true);
		$criteria->compare('rating',$this->rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
