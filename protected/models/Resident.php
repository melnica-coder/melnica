<?php

/**
 * This is the model class for table "resident".
 *
 * The followings are the available columns in table 'resident':
 * @property integer $id
 * @property integer $number
 * @property string $name
 * @property string $short_description
 * @property string $image_filename
 * @property string $occupied_filename
 * @property integer $floor_id
 * @property string $site
 * @property integer $offset_left
 * @property integer $offset_right
 * @property string $region
 *
 * The followings are the available model relations:
 * @property Floor $floor
 */
class Resident extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'resident';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, name, short_description, floor_id, offset_left, offset_right, region', 'required'),
			array('number, floor_id, offset_left, offset_right', 'numerical', 'integerOnly'=>true),
			array('name, image_filename, occupied_filename, site', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, number, name, short_description, image_filename, occupied_filename, floor_id, site, offset_left, offset_right, region', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'floor' => array(self::BELONGS_TO, 'Floor', 'floor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'number' => 'Number',
			'name' => 'Name',
			'short_description' => 'Short Description',
			'image_filename' => 'Image Filename',
			'occupied_filename' => 'Occupied Filename',
			'floor_id' => 'Floor',
			'site' => 'Site',
			'offset_left' => 'Offset Left',
			'offset_right' => 'Offset Right',
			'region' => 'Region',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('number',$this->number);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('short_description',$this->short_description,true);
		$criteria->compare('image_filename',$this->image_filename,true);
		$criteria->compare('occupied_filename',$this->occupied_filename,true);
		$criteria->compare('floor_id',$this->floor_id);
		$criteria->compare('site',$this->site,true);
		$criteria->compare('offset_left',$this->offset_left);
		$criteria->compare('offset_right',$this->offset_right);
		$criteria->compare('region',$this->region,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Resident the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
