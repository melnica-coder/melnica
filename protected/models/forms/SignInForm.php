<?php

class SignInForm extends CFormModel
{
    public $email;
    public $password;
    private $identity;

    public function rules() {
        return array (
            array('password, email', 'required'),
            array('password, email', 'length', 'max' => 50),
            array('email', 'length', 'max' => 50),
            array('password', 'authenticate'),
            array('password,login', 'safe'),
        );
    }

    public function authenticate($attribute, $params) {
        $this->identity = new BackendUserIdentity($this->email, $this->password);
        if (!$this->identity->authenticate()) {
            Yii::app()->user->setFlash('error', 'Wrong email or password');
            $this->addError('password', 'Wrong email or password');
        }
    }

    public function login() {
        if ($this->validate()) {
            Yii::app()->user->login($this->identity, 0); // Forever
            return true;
        }

        return false;
    }
}