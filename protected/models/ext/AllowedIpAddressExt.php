<?php

class AllowedIpAddressExt extends AllowedIpAddress
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getAllowedIpsArray()
    {
        $ips = array();
        $allowedIpAddresses = AllowedIpAddressExt::model()->findAll();
        foreach ($allowedIpAddresses as $allowedIpAddress) {
            $ips [] = $allowedIpAddress->ip;
        }

        return $ips;
    }
}