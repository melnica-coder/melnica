<?php

class ResidentExt extends Resident
{
    public $image;
    public $occupied;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            array(
                'id' => 'ID',
                'number' => 'Номер',
                'name' => 'Название',
                'short_description' => 'Краткое описание',
                'image' => 'Логотип',
                'occupied' => 'Изображение занимаемой площади',
                'floor_id' => 'Этаж',
                'site' => 'Сайт',
                'offset_left' => 'Отступ слева',
                'offset_right' => 'Отступ сверху',
                'region' => 'Регион',
            )
        );
    }
}