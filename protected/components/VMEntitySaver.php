<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class VMEntitySaver extends CComponent
{

	public $object = null;
	public $modelAttribute = 'model';
	public $indexView = 'index';
	public $editView = 'edit';
	public $className = null;
	public $beforeSave = null;
	public $afterSave = null;
	public $beforeRenderIndex = null;
	public $beforeRenderUpdate = null;
	public $safeSaving = true;

	public function __construct($className)
	{
		$this->className = $className;
	}

	public function save()
	{
		$requestParams = Yii::app()->request->getParam($this->className);
		$model = call_user_func(array($this->className, 'model'));
		$model->attributes = $requestParams;

		$primaryKey = $requestParams[$model->tableSchema->primaryKey];


		if (!$primaryKey) {
			$this->object = new $this->className();
		} else {
			$this->object = $model->findByPk($requestParams[$model->tableSchema->primaryKey]);
		}

        if ($this->safeSaving) {

			if ($requestParams == null) {
				throw new CHttpException(400,
					'There are no parameters matching "' . $this->className.'"');
			}

			// Attach not null attributes only
			foreach ($requestParams as $attribute => $value) {
				if ($attribute !== null && $attribute !== $model->tableSchema->primaryKey) {
					$this->object[$attribute] = $requestParams[$attribute];
				}
			}
		} else {
			// Attach all attributes
			$this->object->attributes = $requestParams;
		}

		if ($this->beforeSave) {
			call_user_func($this->beforeSave, $this->object, $requestParams);
		}

		if ($this->object->validate() && $this->object->save()) {

			if ($this->afterSave) {
				call_user_func($this->afterSave, $this->object, $requestParams);
			}

			$this->object->refresh();

			$returnUrl = $this->indexView;

			if ($this->beforeRenderIndex) {
				$params = call_user_func($this->beforeRenderIndex, $this);

				if (isset($params['returnUrl'])) {
					$returnUrl = $params['returnUrl'];
				}
			}
			Yii::app()->request->redirect($returnUrl);
		} else {
			$params = array($this->modelAttribute => $this->object);

			if ($this->beforeRenderUpdate) {
				$returnParams = call_user_func($this->beforeRenderUpdate, $this);
				$params = array_merge($params, $returnParams);
			}

			Yii::app()->controller->render($this->editView, $params);
		}
	}

}

?>
