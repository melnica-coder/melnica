<?php

class BaseBackendController extends CController
{
    public function filters() {
        return array(
            'accessControl',
        );
    }
}