<?php

class EventType {
    CONST TYPE_ANY = 0;
    CONST TYPE_ANNOUNCE = 1;
    CONST TYPE_REPORT = 2;
    CONST TYPE_NOW = 3;
    CONST TYPE_IMPORTANT = 80; // Pseudo type

    public static function getEventTypesList() {
        return array(
            self::TYPE_ANNOUNCE => 'Будет',
            self::TYPE_REPORT => 'Было',
            self::TYPE_NOW => 'Сейчас'
        );
    }

    public static function getMothName($month) {
        $months = array(
            'Января',
            'Февраля',
            'Марта',
            'Апреля',
            'Мая',
            'Июня',
            'Июля',
            'Августа',
            'Сентября',
            'Октября',
            'Ноября',
            'Декабря'
        );
        return $months[$month];
    }

    public static function eventTypeToString($type) {
        $types = self::getEventTypesList();
        if (!array_key_exists($type, $types)) {
            return "[WARNING! Type Not Found! Label '$type']";
        }

        return $types[$type];
    }
}