<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alex
 * Date: 26.07.13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */

class VMImagePreviewColumn extends TbImageColumn
{
	// Default thumbnail width
	public $thumbnail = 150;

	// Default title
	public $headerTitle = 'Image';

	// Detault name value
	public $name = 'image_filename';

	public function init()
	{
		parent::init();

		$this->usePlaceKitten = false;
		$this->usePlaceHoldIt = true;
		$this->placeHoldItSize = $this->thumbnail . 'x' . $this->thumbnail;

		$this->imageOptions = array('width' => $this->thumbnail);
		$this->header = $this->headerTitle;
		$this->imagePathExpression = '$data->{$this->name} != null ? Yii::app()->baseUrl. "/" .$data->{$this->name} : null';
	}
}