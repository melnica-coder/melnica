<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Alex
 * Date: 30.07.13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */

Yii::setPathOfAlias('application.extensions', dirname(__FILE__) . '/../extensions');
Yii::import('application.extensions.file.*');

class VMUploadedMediaManager extends CComponent {
    public $folder = 'media';
    public $prefix = 'media_';

    protected  $file;
    protected  $model;
    protected  $fieldName;
    protected  $filename;

    public function __construct(&$model, $fieldName, $config = array())
    {
        $this->fieldName = $fieldName;
        $this->model = $model;

        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    public static function quickRemove($models, $attribute)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::removeFile($model, $attribute);
            }
        } else {
            self::removeFile($models, $attribute);
        }
    }

    public static function quickSave(CActiveRecord $model, $attribute, $folder = null, $saveToAttribute = null)
    {
        if (!$folder) {
            $suffix = 'Ext';
            $className = lcfirst(get_class($model));
            $extension = substr($className, strlen($className) - strlen($suffix), strlen($suffix));
            if ($extension === $suffix) {
                $className = substr($className, 0, strlen($className) - strlen($suffix));
            }

            $folder = 'media/' . $className . 's';
        }

        if (!$saveToAttribute) {
            $saveToAttribute = $attribute . '_filename';
        }

        $saver = new VMUploadedMediaManager($model, $attribute, array(
            'folder' => $folder,
            'prefix' => $attribute . '_'
        ));

        if ($saver->hasImage()) {
            self::removeFile($model, $saveToAttribute);
        }

        if ($saver->hasImage() && $saver->save()) {
            $model->{$saveToAttribute} = $saver->filename;
            $model->update();
        }
    }

    private static function removeFile(CActiveRecord $model, $saveToAttribute)
    {
        // Remove previously save files
        if ($model->{$saveToAttribute}) {
            $file = new CFile();
            $file = $file->set($model->{$saveToAttribute});
            if ($file->getExists()) {
                $file->delete();
            }
        }
    }

    public function hasImage()
    {
        $this->initFile();
        return $this->file != null;
    }

    private function initFile()
    {
        if (!$this->file) {
            if (isset($this->model->{$this->fieldName})) {
                $this->file = CUploadedFile::getInstance($this->model, $this->fieldName);
            } else {
                $this->file = CUploadedFile::getInstanceByName($this->fieldName);
            }
        }
    }

    public function save()
    {
        $this->initFile();

        if ($this->file) {
            $this->createFolderIfMissing();

            $this->filename = $this->folder . '/' . $this->prefix . $this->model->primaryKey . '.' . $this->extension;

            if (!$this->file->saveAs($this->filename)) {
                return false;
            }
            return true;
        }
        return false;
    }

    private function createFolderIfMissing()
    {
        if (!file_exists($this->folder)) {
            mkdir($this->folder, 0777, true);
        }
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getExtension()
    {
        return strtolower($this->file->extensionName);
    }
}