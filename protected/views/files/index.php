<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить файл',
                'type' => 'primary',
                'url' => $this->createUrl('files/create')
            )
        );
    ?>
</div>

<h1>Файлы</h1>

<p>
</p>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'comment',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'name' => 'label',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>
</p>