<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить событие',
                'type' => 'primary',
                'url' => $this->createUrl('events/create')
            )
        );
    ?>
</div>

<h1>События</h1>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'header' => 'Иконка',
                        'class' => 'VMImagePreviewColumn',
                        'name' => 'icon_filename',
                        'headerHtmlOptions' => array('class' => 'span3')
                    ),
                    array(
                        'name' => 'header',
                        'headerHtmlOptions' => array('class' => 'span3')
                    ),
                    array(
                        'name' => 'date',
                        'headerHtmlOptions' => array(
                            'class' => 'span3',
                            'dateFormat' => 'dd-mm-yyyy'
                        )
                    ),
                    array(
                        'header' => 'Тип',
                        'value' => '$data->type == EventType::TYPE_ANNOUNCE ? "Будет" : $data->type == EventType::TYPE_REPORT? "Было" : "Сейчас"',
                        'headerHtmlOptions' => array('class' => 'span3'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>