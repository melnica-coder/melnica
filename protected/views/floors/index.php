<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить этаж',
                'type' => 'primary',
                'url' => $this->createUrl('floors/create')
            )
        );
    ?>
</div>

<h1>Этажи</h1>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'number',
                        'headerHtmlOptions' => array('class' => 'span1')
                    ),
                    array(
                        'name' => 'total_space',
                        'headerHtmlOptions' => array('class' => 'span8')
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>