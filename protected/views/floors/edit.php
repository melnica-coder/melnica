<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('floors/save'),
        )
    );
?>
    <fieldset>
        <legend>Добавить / Редактировать этаж</legend>

        <?= $form->errorSummary($model); ?>
        <?= $form->textFieldRow($model, 'number', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'total_space', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'free_space', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'occupied_space', array('class' => 'span3')); ?>
        <?= $form->textFieldRow($model, 'image_height', array('class' => 'span3')); ?>

        <hr />
        <?= $form->dropDownListRow($model, 'image_gallery_id', $imageGalleries, array('class' => 'span3')); ?>
        <?= $form->fileFieldRow($model, 'background', array('class' => 'span3', 'hint' => 'Разрешение: 1024 x 560 px')); ?>
        <?= $form->fileFieldRow($model, 'occupied', array('class' => 'span3', 'hint' => 'Разрешение: 1024 x 560 px')); ?>
        <hr />

        <?= $form->hiddenField($model, 'id'); ?>

        <div class="form-actions">
            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => 'Сохранить'
                    )
                );
            ?>

            <?php
                $this->widget(
                    'TbButton',
                    array(
                        'buttonType' => 'link',
                        'url' => $this->createUrl('floors/index'),
                        'label' => 'Отмена'
                    )
                );
            ?>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>