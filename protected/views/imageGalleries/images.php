<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Назад к списку галерей',
                'type' => 'default',
                'url' => $this->createUrl('imageGalleries/index', array('type' => $imageGallery->type))
            )
        );
    ?>
</div>

<h1>Галерея "<?= CHtml::encode($imageGallery->name); ?>"</h1>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'id',
                        'header' => '#',
                        'headerHtmlOptions' => array('class' => 'span1')
                    ),
                    array(
                        'class' => 'VMImagePreviewColumn',
                        'name' => 'image_filename',
                        'headerHtmlOptions' => array('class' => 'span8')
                    ),
                    array(
                        'class' => 'TbButtonColumn',
                        'header' => 'Действия',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{delete}',
                        'buttons' => array(
                            'delete' => array(
                                'label' => 'Удалить',
                                'url' => 'Yii::app()->createUrl("imageGalleries/deleteImage", array("id" => $data->id))',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>
</p>

<hr />
<h2>Загрузить изображение</h2>
<hr />

<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('imageGalleries/addImage'),
        )
    );
?>
<?= $form->errorSummary($model); ?>
<?= $form->fileFieldRow($model, 'image', array('class' => 'span3')); ?>

<?= $form->hiddenField($model, 'image_gallery_id'); ?>
<?= $form->hiddenField($model, 'id'); ?>

<div class="form-actions">
    <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Загрузить'
            )
        );
    ?>
</div>
<?php $this->endWidget(); ?>