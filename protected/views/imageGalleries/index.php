<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить галерею',
                'type' => 'primary',
                'url' => $this->createUrl('imageGalleries/create', array('type' => $type))
            )
        );
    ?>
</div>

<h1>Галереи для раздела "<?= CHtml::encode(ImageGalleryType::toString($type)); ?>"</h1>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'header' => '#',
                        'name' => 'id',
                        'headerHtmlOptions' => array('class' => 'span1')
                    ),
                    array(
                        'class' => 'TbEditableColumn',
                        'name' => 'name',
                        'editable' => array(
                            'url' => $this->createUrl('imageGalleries/editable'),
                        ),
                        'headerHtmlOptions' => array('class' => 'span8'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{images} {delete} {update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                            'delete' => array(
                                'label' => 'Удалить',
                                'url' => 'Yii::app()->createUrl("imageGalleries/deleteImageGallery", array("id" => $data->id))',
                            ),
                            'images' => array(
                                'label' => 'Редактировать изображения',
                                'icon' => 'th-list',
                                'url' => 'Yii::app()->createUrl("imageGalleries/images", array("imageGalleryId" => $data->id))',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>
</p>