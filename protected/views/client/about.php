<div id="about">
    <div class="img-block img-block-1">
        <div class="header">
            <?= ClientRenderHelper::getTextByLabel('about_block1_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('about_block1_header_text') ?>
        </div>
    </div>

    <div class="inside-loft">
        <div class="flag"></div>

        <div class="heading-with-line-light">
            <span class="line"></span>
            <span>На территории проекта</span>
            <span class="line"></span>
        </div>

        <div class="on-territory">
            <span class="territory">
                <img class="parking" src="css/images/about-1024/parking.png" />
                Удобная парковка
            </span>
            <span class="territory">
                <img class="information" src="css/images/about-1024/information.png" />
                Интересные соседи
            </span>
            <span class="territory">
                <img class="aesthetics" src="css/images/about-1024/aesthetics.png" />
                Уникальная эстетика
            </span>
            <span class="territory">
                <img class="cafe" src="css/images/about-1024/cafe.png" />
                Кафе и коворкинг
            </span>

            <span class="territory">
                <img class="space" src="css/images/about-1024/space.png" />
                Пространство<br />
                для событий
            </span>
            <span class="territory">
                <img class="neighbours" src="css/images/about-1024/neighbours.png" />
                Пространство<br />
                для переговоров
            </span>
            <span class="territory">
                <img class="communications" src="css/images/about-1024/communications.png" />
                Простоянная<br />
                активность СМИ
            </span>
            <span class="territory">
                <img class="idea" src="css/images/about-1024/idea.png" />
                Постоянная<br />
                творческая активность
            </span>

            <br class="clear" />
        </div>

        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('about_block1_place_text') ?>
        </div>
    </div>

    <div class="img-block img-block-2">
        <div class="header">
            <?= ClientRenderHelper::getTextByLabel('about_block2_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('about_block2_header_text') ?>
        </div>
    </div>

    <div class="environment">
        <div class="pin"></div>
        <div class="heading-with-line-light">
            <span class="line"></span>
            <span>Локация и окружение</span>
            <span class="line"></span>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('about_block2_environment_text') ?>
        </div>
        <div class="icons">
            <span class="icon" onclick="showMelnica()">
                <img class="melnica" src="css/images/icons/melnica.png" />
                Мельница
            </span>
            <span class="icon" onclick="showParks()">
                <img class="park" src="css/images/icons/park.png" />
                Парки
            </span>
            <span class="icon" onclick="showCafe()">
                <img class="cafe" src="css/images/icons/cafe.png" />
                Кафе
            </span>
            <span class="icon" onclick="showCinema()">
                <img class="cinema" src="css/images/icons/cinema.png" />
                Кино
            </span>
            <span class="icon" onclick="showGallery()">
                <img class="gallery" src="css/images/icons/gallery.png" />
                Галереи
            </span>
            <span class="icon" onclick="showMuseum()">
                <img class="museum" src="css/images/icons/museum.png" />
                Архитектура
            </span>
            <span class="icon" onclick="showMusic()">
                <img class="music" src="css/images/icons/music.png" />
                Музыка
            </span>
            <span class="icon" onclick="showNice()">
                <img class="nice" src="css/images/icons/nice.png" />
                Тут интересно
            </span>
            <br class="clear" />
        </div>
    </div>

    <div id="map" class="map"></div>

    <div class="img-block img-block-3">
        <div class="year">
            <span class="line"></span>
            <span>1910</span>
            <span class="line"></span>
        </div>
        <div class="header padding1">
            <?= ClientRenderHelper::getTextByLabel('about_block3_header_header') ?>
        </div>
        <div class="text padding2">
            <?= ClientRenderHelper::getTextByLabel('about_block3_header_text') ?>
        </div>
    </div>

    <div class="text-block-wrapper">
        <div class="text-block">
            <table class="clean">
                <tr>
                    <td>
                        <div class="image left image-1"></div>
                    </td>
                    <td class="separator"></td>
                    <td>
                        <div class="header">
                            <?= ClientRenderHelper::getTextByLabel('about_block3_content1_header') ?>
                        </div>
                        <div class="text">
                            <?= ClientRenderHelper::getTextByLabel('about_block3_content1_text') ?>
                        </div>
                    <td>
                </tr>
            </table>
        </div>
    </div>

    <div class="text-block-wrapper highlight">
        <div class="text-block quote center">
            <?= ClientRenderHelper::getTextByLabel('about_block3_content2_quote') ?>
            <br />
            <br />
            <span class="highlight"><?= ClientRenderHelper::getTextByLabel('about_block3_content2_author') ?></span>
        </div>
    </div>

    <div class="text-block-wrapper">
        <div class="text-block">
            <table class="clean">
                <tr>
                    <td>
                        <div class="header">
                            <?= ClientRenderHelper::getTextByLabel('about_block3_content3_header') ?>
                        </div>
                        <div class="text">
                            <?= ClientRenderHelper::getTextByLabel('about_block3_content3_text') ?>
                        </div>
                    </td>
                    <td class="separator"></td>
                    <td>
                        <div class="image right image-2"></div>
                        <br class="clear" />
                    <td>
                </tr>
            </table>
        </div>
    </div>

    <div class="img-block img-block-4">
        <div class="year">
            <span class="line"></span>
            <span>1930</span>
            <span class="line"></span>
        </div>
        <div class="header padding1">
            <?= ClientRenderHelper::getTextByLabel('about_block4_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('about_block4_header_text') ?>
        </div>
    </div>

    <div class="text-block-wrapper">
        <div class="text-block">
            <table class="clean">
                <tr>
                    <td>
                        <div class="image left image-3"></div>
                    </td>
                    <td class="separator"></td>
                    <td>
                        <div class="header">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content1_header') ?>
                        </div>
                        <div class="text">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content1_text') ?>
                        </div>
                    <td>
                </tr>
            </table>
        </div>
    </div>

    <div class="text-block-wrapper highlight">
        <div class="text-block">
            <table class="clean">
                <tr>
                    <td>
                        <div class="header">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content2_header') ?>
                        </div>
                        <div class="text">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content2_text') ?>
                        </div>
                    </td>
                    <td class="separator"></td>
                    <td>
                        <div class="image right image-4"></div>
                        <br class="clear" />
                    <td>
                </tr>
            </table>
        </div>
    </div>

    <div class="text-block-wrapper">
        <div class="text-block last">
            <table class="clean">
                <tr>
                    <td>
                        <div class="image left image-5"></div>
                    </td>
                    <td class="separator"></td>
                    <td>
                        <div class="header">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content3_header') ?>
                        </div>
                        <div class="text">
                            <?= ClientRenderHelper::getTextByLabel('about_block4_content3_text') ?>
                        </div>
                    <td>
                </tr>
            </table>
        </div>
    </div>

    <!--<div class="footer-links">
        <table class="clean">
            <tr>
                <td class="border">
                    <a href="#">услуги проекта</a>
                </td>
                <td>
                    <a href="#">планировка этажей</a>
                </td>
            </tr>
        </table>
    </div>-->
</div>

<script type="text/javascript">
    function initializeMap(mapFile, preserveViewport) {
        var nsk = new google.maps.LatLng(55.0199788,82.9162304);
        var mapOptions = {
            zoom: 16,
            center: nsk
        }

        styles = [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-77}]},{"featureType":"road"}];

        mapOptions = $.extend({
            scrollwheel: false,
            styles: styles
        }, mapOptions);

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var ctaLayer = new google.maps.KmlLayer(
            'http://melnicaloft.ru/media/' + mapFile,
            {
                /*suppressInfoWindows: true,*/
                map: map,
                preserveViewport: preserveViewport
            }
        );
    }

    function showMelnica() {
        initializeMap('melnica.kml', true);
    }

    function showParks() {
        initializeMap('parks.kml', false);
    }

    function showCafe() {
        initializeMap('cafe.kml', false);
    }

    function showCinema() {
        initializeMap('cinema.kml', false);
    }

    function showGallery() {
        initializeMap('gallery.kml', false);
    }

    function showMuseum() {
        initializeMap('museum.kml', false);
    }

    function showMusic() {
        initializeMap('music.kml', false);
    }

    function showNice() {
        initializeMap('nice.kml', false);
    }

    $(function() {
        // Google maps
        initializeMap('map-1.kml', true);

        // Flying-in Images
        var lastScrollTop = 0;
        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();

            if (scrollTop <= lastScrollTop){
                return;
            }
            lastScrollTop = scrollTop;

            var windowHeight = $(window).height();
            var marginMax = 200;

            $('#about div.image').map(function() {
                var node = $(this);
                var nodeTop = node.offset().top;
                var top = Math.min(Math.max(nodeTop - windowHeight * 0.2 - scrollTop, 0), windowHeight);

                if (nodeTop - scrollTop < windowHeight / 1.5) {
                    if (node.hasClass('arrived')) {
                        return;
                    }
                    node.addClass('arrived');

                    if (node.hasClass('left')) {
                        node.css('margin-left', -marginMax);
                        node.animate(
                            {
                                'opacity': '1.0',
                                'margin-left': 0
                            },
                            {
                                'duration': 500,
                                'easing': 'easeOutBack'
                            },
                            function() {}
                        );
                    } else {
                        node.css('margin-right', -marginMax);
                        node.animate(
                            {
                                'opacity': '1.0',
                                'margin-right': 0
                            },
                            {
                                'duration': 500,
                                'easing': 'easeOutBack'
                            },
                            function() {}
                        );
                    }
                } else {
                    if (node.hasClass('arrived')) {
                        return;
                    }
                    node.css('opacity', 0.01);
                }
            })
        });
    });
</script>
