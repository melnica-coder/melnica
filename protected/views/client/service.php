<?php
    $download1 = ClientRenderHelper::getUploadedFileByLabel('service_long_term_rent_1');
    $download2 = ClientRenderHelper::getUploadedFileByLabel('service_presentation_loft_1');
    $download3 = ClientRenderHelper::getUploadedFileByLabel('service_short_term_rent_1');
    $download4 = ClientRenderHelper::getUploadedFileByLabel('service_presentation_place_1');
    $download5 = ClientRenderHelper::getUploadedFileByLabel('service_art_groups_and_business_interaction_1');
    $download6 = ClientRenderHelper::getUploadedFileByLabel('service_members_introduction_1');
    $download7 = ClientRenderHelper::getUploadedFileByLabel('service_art_groups_and_projects_1');
    $download8 = ClientRenderHelper::getUploadedFileByLabel('service_presentation_loft_2');
?>

<div id="local-navigation" class="local-navigation absolute">
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-1">аренда</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-2">услуги</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-3">партнерство</a>
    <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-4">события</a>
</div>

<div id="service">
    <div class="local-navigation">
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-1">аренда</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-2">услуги</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-3">партнерство</a>
        <a class="local-navigation-item soft-hover" href="<?= ClientRenderHelper::pageHref('service') ?>#block-4">события</a>
    </div>

    <div class="img-block img-block-1" id="block-1">
        <div class="number">
            <span class="line"></span>
            <span>1</span>
            <span class="line"></span>
        </div>
        <div class="header">
            <?= ClientRenderHelper::getTextByLabel('service_block1_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('service_block1_header_text') ?>
        </div>
    </div>

    <div class="text-block">
        <table class="clean">
            <tr>
                <td class="col1">
                    <div class="header">
                        <?= ClientRenderHelper::getTextByLabel('service_block1_content_header') ?>
                    </div>
                    <div class="text">
                        <?= ClientRenderHelper::getTextByLabel('service_block1_content_text') ?>
                        <br />
                        <br />
                        <br />
                        <a class="link" href="<?= ClientRenderHelper::pageHref('planing') ?>">Смотреть планировки</a>
                    </div>
                </td>
                <td class="col2">
                    <a href="<?= $download1[0] ?>" download="<?= $download1[1] ?>"  class="pdf-black">заявка<br />на аренду</a>
                    <a href="<?= $download2[0] ?>" download="<?= $download2[1] ?>"  class="pdf-orange">Презентация<br /> проекта.PDF</a>
                    <div class="highlight"><?= ClientRenderHelper::getTextByLabel('service_block1_space') ?></div>
                </td>
            </tr>
        </table>
    </div>

    <div class="img-block img-block-2" id="block-2">
        <div class="number">
            <span class="line"></span>
            <span>2</span>
            <span class="line"></span>
        </div>
        <div class="header">
            <?= ClientRenderHelper::getTextByLabel('service_block2_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('service_block2_header_text') ?>
        </div>
    </div>

    <div class="text-block">
        <table class="clean">
            <tr>
                <td class="col1">
                    <div class="header">
                        <?= ClientRenderHelper::getTextByLabel('service_block2_content_header') ?>
                    </div>
                    <div class="text">
                        <?= ClientRenderHelper::getTextByLabel('service_block2_content_text') ?>
                    </div>
                </td>
                <td class="col2">
                    <a href="<?= $download5[0] ?>" download="<?= $download5[1] ?>" class="pdf-black" style="width: 250px;">заявка<br />на услуги</a>
                    <a href="<?= $download6[0] ?>" download="<?= $download6[1] ?>" class="pdf-orange">презентация<br /> услуг.PDF</a>
                </td>
            </tr>
        </table>
    </div>

    <div class="img-block img-block-3" id="block-3">
        <div class="number">
            <span class="line"></span>
            <span>3</span>
            <span class="line"></span>
        </div>
        <div class="header padding1">
            <?= ClientRenderHelper::getTextByLabel('service_block3_header_header') ?>
        </div>
        <div class="text padding2">
            <?= ClientRenderHelper::getTextByLabel('service_block3_header_text') ?>
        </div>
    </div>

    <div class="text-block">
        <table class="clean">
            <tr>
                <td class="col1">
                    <div class="header">
                        <?= ClientRenderHelper::getTextByLabel('service_block3_content_header') ?>
                    </div>
                    <div class="text">
                        <?= ClientRenderHelper::getTextByLabel('service_block3_content_text') ?>
                    </div>
                    <br />
                    <br />
                    <br />
                    <a class="link" href="<?= ClientRenderHelper::pageHref('events', array('id' => EventType::TYPE_IMPORTANT)) ?>">Смотреть события</a>
                </td>
                <td class="col2">
                    <a href="<?= $download7[0] ?>" download="<?= $download7[1] ?>" class="pdf-black" style="width: 250px">стать<br />партнером</a>
                    <!--<a href="<?= $download8[0] ?>" download="<?= $download8[1] ?>" class="pdf-orange">презентация<br /> лофта.PDF</a>-->
                </td>
            </tr>
        </table>
    </div>

    <div class="img-block img-block-4" id="block-4">
        <div class="number">
            <span class="line"></span>
            <span>4</span>
            <span class="line"></span>
        </div>
        <div class="header padding1">
            <?= ClientRenderHelper::getTextByLabel('service_block4_header_header') ?>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('service_block4_header_text') ?>
        </div>
    </div>

    <div class="text-block">
        <table class="clean">
            <tr>
                <td class="col1">
                    <div class="header">
                        <?= ClientRenderHelper::getTextByLabel('service_block4_content_header') ?>
                    </div>
                    <div class="text">
                        <?= ClientRenderHelper::getTextByLabel('service_block4_content_text') ?>
                    </div>
                </td>
                <td class="col2">
                    <a href="<?= $download3[0] ?>" download="<?= $download3[1] ?>" class="pdf-black">заявка<br />на событие</a>
                    <a href="<?= $download4[0] ?>" download="<?= $download4[1] ?>" class="pdf-orange">Презентация<br /> площадки.PDF</a>
                    <div class="highlight"><?= ClientRenderHelper::getTextByLabel('service_block3_space') ?></div>
                </td>
            </tr>
        </table>
    </div>
</div>

<script>
    $(function() {
        function updateLocalNavigation() {
            if ($(window).scrollTop() > 180) {
                $('#local-navigation').show();
            } else {
                $('#local-navigation').hide();
            }
        }

        $(window).scroll(function() {
            updateLocalNavigation();
        });
        updateLocalNavigation();
    });
</script>