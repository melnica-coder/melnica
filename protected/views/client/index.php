<div id="index">
    <table class="clean tiles">
        <tr>
            <td class="squared tile-half banner brick-1" colspan="2" rowspan="2">
                <div><a class="square-wrapper" href="<?= ClientRenderHelper::pageHref('service') ?>"></a></div>
            </td>
            <td class="rect tile-half banner brick-2" colspan="2">
                <div><a class="square-wrapper" href="<?= ClientRenderHelper::pageHref('about') ?>"></a></div>
            </td>
        </tr>
        <tr>
            <td class="rect tile-half banner brick-3" colspan="2">
                <div><a class="square-wrapper" href="<?= ClientRenderHelper::pageHref('planing') ?>"></a></div>
            </td>
        </tr>

        <?php
            $counter = 0;
            foreach ($latestEvents as $event) {
                $styleIdx = (($counter) % 3) + 1;
                if ($counter % 2 == 0) {
                    echo '<tr>';
                }

                if ($counter % 4 == 0 || $counter % 4 == 1) {
        ?>
                <td class="soft-hover squared tile-half" colspan="2">
                    <a class="square-wrapper" href="<?= ClientRenderHelper::pageHref('event', array('id' => $event->id)) ?>">
                        <table class="clean tiles">
                            <tr>
                                <td class="squared tile-half brick brick-6" style="background-image: url('<?= $event->icon_filename ?>')">
                                    <div class="arrow arrow-left-event-<?= $styleIdx ?>"></div>
                                </td>
                                <td class="squared tile-half event event-<?= $styleIdx ?>">
                                    <div class="event-wrapper">
                                        <div class="type <?= ClientRenderHelper::eventTypeToClassName($event->type) ?>">
                                            <?= EventType::eventTypeToString($event->type) ?>
                                        </div>
                                        <div class="header">
                                            <?= ClientRenderHelper::_($event->header) ?>
                                        </div>
                                        <div class="date">
                                            <?= $event->type == EventType::TYPE_NOW ? 'до ' : '' ?>
                                            <?= date('j', strtotime($event->date)); ?>
                                            <?= EventType::getMothName(date('n', strtotime($event->date)) - 1); ?>
                                            <?= date('Y', strtotime($event->date)); ?> г.
                                        </div>
                                        <div class="description">
                                            <?= ClientRenderHelper::_($event->text_1) ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </a>
                </td>
        <?php
                }  else {
        ?>
                <td class="soft-hover squared tile-half" colspan="2">
                    <a class="square-wrapper" href="<?= ClientRenderHelper::pageHref('event', array('id' => $event->id)) ?>">
                        <table class="clean tiles">
                            <tr>
                                <td class="squared tile-half event event-<?= $styleIdx ?>">
                                    <div class="event-wrapper">
                                        <div class="type <?= ClientRenderHelper::eventTypeToClassName($event->type) ?>">
                                            <?= EventType::eventTypeToString($event->type) ?>
                                        </div>
                                        <div class="header">
                                            <?= ClientRenderHelper::_($event->header) ?>
                                        </div>
                                        <div class="date">
                                            <?= $event->type == EventType::TYPE_NOW ? 'до ' : '' ?>
                                            <?= date('j', strtotime($event->date)); ?>
                                            <?= EventType::getMothName(date('n', strtotime($event->date)) - 1); ?>
                                            <?= date('Y', strtotime($event->date)); ?> г.
                                        </div>
                                        <div class="description">
                                            <?= ClientRenderHelper::_($event->text_1) ?>
                                        </div>
                                    </div>
                                </td>
                                <td class="squared tile-half brick brick-6" style="background-image: url('<?= $event->icon_filename ?>')">
                                    <div class="arrow arrow-right-event-<?= $styleIdx ?>"></div>
                                </td>
                            </tr>
                        </table>
                    </a>
                </td>
        <?php
                }

                if ($counter % 2 == 1) {
                    echo '</tr>';
                }

                $counter++;
            }

            if ($counter % 2 != 0) {
        ?>
                <td colspan="2"></td>
            </tr>
        <?php
            }
        ?>
    </table>

    <!--<a id="more-news">
        Еще новости
    </a>-->
</div>
