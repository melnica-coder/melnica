<script type="text/javascript">
    function initializeMap(mapFile, preserveViewport) {
        $('#map').html('');
        var nsk = new google.maps.LatLng(55.0199788,82.9162304);
        var mapOptions = {
            zoom: 16,
            center: nsk
        }

        styles = [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-77}]},{"featureType":"road"}];

        mapOptions = $.extend({
            scrollwheel: false,
            styles: styles
        }, mapOptions);

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var ctaLayer = new google.maps.KmlLayer(
            'http://melnicaloft.ru/media/' + mapFile,
            {
                /*suppressInfoWindows: true,*/
                map: map,
                preserveViewport: preserveViewport
            }
        );
    }

    $(function() {
        initializeMap('map-1.kml', true);
    });

    function showMelnica() {
        initializeMap('melnica.kml', true);
    }

    function showParks() {
        initializeMap('parks.kml', false);
    }

    function showCafe() {
        initializeMap('cafe.kml', false);
    }

    function showCinema() {
        initializeMap('cinema.kml', false);
    }

    function showGallery() {
        initializeMap('gallery.kml', false);
    }

    function showMuseum() {
        initializeMap('museum.kml', false);
    }

    function showMusic() {
        initializeMap('music.kml', false);
    }

    function showNice() {
        initializeMap('nice.kml', false);
    }
</script>

<div id="contacts">
    <div class="header">
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('contacts_we_are_open_for_cooperation_1') ?>
        </div>
        <a href="<?= ClientRenderHelper::pageHref('contacts') ?>#map" class="pin"></a>
        <div class="address">
            <?= ClientRenderHelper::getContactByLabel('full_address_1') ?>
        </div>
        <div class="phones">
            <span class="phone right-spacing"><?= ClientRenderHelper::getContactByLabel('city_phone_1') ?></span>
            <span class="phone"><?= ClientRenderHelper::getContactByLabel('city_phone_2') ?></span>
        </div>
    </div>
    <div class="people">
        <table class="clean">
            <tr>
                <td>
                    <div class="role">Арт-директор</div>
                    <div class="name"><?= ClientRenderHelper::getContactByLabel('contacts_art_director_name_1') ?></div>
                    <div class="phone"><?= ClientRenderHelper::getContactByLabel('contacts_art_director_phone_1') ?></div>
                    <a href="mailto:<?= ClientRenderHelper::getContactByLabel('contacts_art_director_email_1') ?>" class="mail"><?= ClientRenderHelper::getContactByLabel('contacts_art_director_email_1') ?></a>
                </td>
                <td>
                    <div class="role">Коммерческий директор</div>
                    <div class="name"><?= ClientRenderHelper::getContactByLabel('contacts_sales_manager_name_1') ?></div>
                    <div class="phone"><?= ClientRenderHelper::getContactByLabel('contacts_sales_manager_phone_1') ?></div>
                    <a href="mailto:<?= ClientRenderHelper::getContactByLabel('contacts_sales_manager_email_1') ?>" class="mail"><?= ClientRenderHelper::getContactByLabel('contacts_sales_manager_email_1') ?></a>
                </td>
            </tr>
        </table>
    </div>
    <div class="documents">
        <div class="heading-with-line-dark">
            <span class="line"></span>
            <span>Документы и презентации</span>
            <span class="line"></span>
        </div>
        <div class="pdfs">
            <?php
                $download1 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_rent_1');
                $download2 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_event_1');
                $download3 = ClientRenderHelper::getUploadedFileByLabel('contacts_application_collaboration_1');
                $download4 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_loft_1');
                $download5 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_place_1');
                $download6 = ClientRenderHelper::getUploadedFileByLabel('contacts_presentation_members_1');
            ?>
            <a href="<?= $download1[0] ?>" download="<?= $download1[1] ?>" class="pdf-black">Заявка<br /> на аренду</a>
            <a href="<?= $download2[0] ?>" download="<?= $download2[1] ?>" class="pdf-black">Заявка<br /> на событие</a>
            <!--<a href="<?= $download3[0] ?>" download="<?= $download3[1] ?>" class="pdf-black">Заявка<br /> на колаборацию</a>-->
            <a href="<?= $download4[0] ?>" download="<?= $download4[1] ?>" class="pdf-orange">Презентация<br /> проекта.PDF</a>
            <!--<a href="<?= $download5[0] ?>" download="<?= $download5[1] ?>" class="pdf-orange">Презентация<br /> площадки.PDF</a>
            <a href="<?= $download6[0] ?>" download="<?= $download6[1] ?>" class="pdf-orange">Презентация<br /> участников.PDF</a>-->
        </div>
    </div>
    <div class="environment">
        <div class="pin"></div>
        <div class="heading-with-line-light">
            <span class="line"></span>
            <span>Локация и окружение</span>
            <span class="line"></span>
        </div>
        <div class="text">
            <?= ClientRenderHelper::getTextByLabel('contacts_environment_1') ?>
        </div>
        <div class="icons">
            <span class="icon" onclick="showMelnica()">
                <img class="melnica" src="css/images/icons/melnica.png" />
                Мельница
            </span>
            <span class="icon" onclick="showParks()">
                <img class="park" src="css/images/icons/park.png" />
                Парки
            </span>
            <span class="icon" onclick="showCafe()">
                <img class="cafe" src="css/images/icons/cafe.png" />
                Кафе
            </span>
            <span class="icon" onclick="showCinema()">
                <img class="cinema" src="css/images/icons/cinema.png" />
                Кино
            </span>
            <span class="icon" onclick="showGallery()">
                <img class="gallery" src="css/images/icons/gallery.png" />
                Галереи
            </span>
            <span class="icon" onclick="showMuseum()">
                <img class="museum" src="css/images/icons/museum.png" />
                Архитектура
            </span>
            <span class="icon" onclick="showMusic()">
                <img class="music" src="css/images/icons/music.png" />
                Музыка
            </span>
            <span class="icon" onclick="showNice()">
                <img class="nice" src="css/images/icons/nice.png" />
                Тут интересно
            </span>
            <br class="clear" />
        </div>
    </div>

    <div id="map" class="map"></div>

    <!--<div class="footer-links">
        <table class="clean">
            <tr>
                <td class="border">
                    <a href="<?= ClientRenderHelper::pageHref('service') ?>">предложение проекта</a>
                </td>
                <td>
                    <a href="<?= ClientRenderHelper::pageHref('planing') ?>">планировка этажей</a>
                </td>
            </tr>
        </table>
    </div>-->
</div>