<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить ip',
                'type' => 'primary',
                'url' => $this->createUrl('allowedIpAddresses/create')
            )
        );
    ?>
</div>

<h1>Разрешенные ip-адреса</h1>

<p>
    <b>Список адресов с которых разрешен вход в панель администратора</b>.<br />
    <b>Вход с других адресов закрыт в целях безопасности.</b><br />
    <br />
    <ul>
        <li>При удалении ip, с которого вы выходите в интернет, из данного списка - доступ для Вас к панели администратора будет сразу прекращен.</li>
        <li>При удалении всех ip, доступ в панель администратора будет закрыт всем - потребуется помощь администратора сайта.</li>
    </ul>
</p>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'header' => 'ip',
                        'name' => 'ip',
                        'headerHtmlOptions' => array('class' => 'span8'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{delete} {update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                            'delete' => array(
                                'label' => 'Удалить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>