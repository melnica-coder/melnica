<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('allowedIpAddresses/save'),
        )
    );
?>

<h1>Добавить / Редактировать ip-адрес</h1>

<p>
    <ul>
        <li>Указываемый ip-адрес должен быть <b>белым ip</b>.</li>
        <li>Символы * - не поддерживаются (т.е. 192.168.0.* не будет работать).</li>
    </ul>
</p>

<hr />

<fieldset>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'ip', array('class' => 'span3')); ?>

    <?= $form->hiddenField($model, 'id'); ?>

    <div class="form-actions">
        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Сохранить'
                )
            );
        ?>

        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'link',
                    'url' => $this->createUrl('allowedIpAddresses/index'),
                    'label' => 'Отмена'
                )
            );
        ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>