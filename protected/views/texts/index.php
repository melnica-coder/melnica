<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить текстовую вставку',
                'type' => 'primary',
                'url' => $this->createUrl('texts/create')
            )
        );
    ?>
</div>

<h1>Текстовые вставки</h1>
<p>
    <b>Блоки текста, которые можно отредактировать.</b>
</p>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'comment',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'name' => 'label',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'name' => 'text',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>