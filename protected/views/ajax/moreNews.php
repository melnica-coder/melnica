<div id="events-carousel" class="other-news" style="text-align: left; margin-top: 3px; padding-left: 8px">
<?php
    foreach ($events as $event) {
?>
    <a href="<?= ClientRenderHelper::pageHref('event', array('id' => $event->id)) ?>" class="news" style="text-align: center">
        <div class="image image-1" style="background-image: url('<?= $event->icon_filename ?>')">
            <div class="arrow-top"></div>
        </div>
        <div class="header">
            <?= ClientRenderHelper::_($event->header) ?>
        </div>
        <div class="date">
            <?= $event->type == EventType::TYPE_NOW ? 'до ' : '' ?>
            <?= date('j', strtotime($event->date)); ?>
            <?= EventType::getMothName(date('n', strtotime($event->date)) - 1); ?>
            <?= date('Y', strtotime($event->date)); ?> г.
        </div>
        <div class="description">
            <?= ClientRenderHelper::_($event->text_1, 25) ?>
        </div>
        <div class="type <?= ClientRenderHelper::eventTypeToClassName($event->type) ?>">
            <?= EventType::eventTypeToString($event->type) ?>
        </div>
    </a>
<?php
    }
?>
    <span class="count" data-count="<?= count($events) ?>"></span>
</div>