<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('contacts/save'),
        )
    );
?>

<h1>Добавить / Редактировать контакт</h1>

<hr />

<fieldset>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'label', array('hint' => 'Уникальное системное имя (информация для программиста - не менять)', 'class' => 'span3')); ?>
    <?= $form->textareaRow($model, 'comment', array('hint' => 'Расположение на сайте', 'class' => 'span6', 'rows' => 2, 'style' => 'resize: none')); ?>
    <?= $form->textFieldRow($model, 'contact', array('hint' => 'Контактная информация', 'class' => 'span3')); ?>

    <?= $form->hiddenField($model, 'id'); ?>

    <div class="form-actions">
        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Сохранить'
                )
            );
        ?>

        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'link',
                    'url' => $this->createUrl('contacts/index'),
                    'label' => 'Отмена'
                )
            );
        ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>