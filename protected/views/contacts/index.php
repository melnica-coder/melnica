<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить контакт',
                'type' => 'primary',
                'url' => $this->createUrl('contacts/create')
            )
        );
    ?>
</div>

<h1>Контакты</h1>
<p>
    <b>Телефоны и email - указанные на сайте.</b>
</p>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'comment',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'name' => 'label',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'name' => 'contact',
                        'headerHtmlOptions' => array('class' => 'span5'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            )
                        ),
                    ),
                ),
            )
        );
    ?>

</p>