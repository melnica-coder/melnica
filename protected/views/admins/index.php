<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить администратора',
                'type' => 'primary',
                'url' => $this->createUrl('admins/create')
            )
        );
    ?>
</div>

<h1>Администраторы</h1>

<p>
    <b>Список пользователей, которым разрешен вход в панель администратора</b>.
</p>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'header' => '#',
                        'name' => 'id',
                        'headerHtmlOptions' => array('class' => 'span1')
                    ),
                    array(
                        'header' => 'email',
                        'class' => 'TbEditableColumn',
                        'name' => 'email',
                        'editable' => array(
                            'url' => $this->createUrl('admins/editable'),
                        ),
                        'headerHtmlOptions' => array('class' => 'span8'),
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{delete} {update}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                            'delete' => array(
                                'label' => 'Удалить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>