<?php
    $form = $this->beginWidget(
        'TbActiveForm',
        array(
            'type' => 'horizontal',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'action' => $this->createUrl('admins/save'),
        )
    );
?>

<fieldset>
    <legend>Добавить / Редактировать администратора</legend>

    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'email', array('class' => 'span3')); ?>
    <?= $form->passwordFieldRow($model, 'password', array('value' => '', 'class' => 'span3')); ?>

    <?= $form->hiddenField($model, 'id'); ?>

    <div class="form-actions">
        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Сохранить'
                )
            );
        ?>

        <?php
            $this->widget(
                'TbButton',
                array(
                    'buttonType' => 'link',
                    'url' => $this->createUrl('admins/index'),
                    'label' => 'Отмена'
                )
            );
        ?>
    </div>
</fieldset>

<?php $this->endWidget(); ?>