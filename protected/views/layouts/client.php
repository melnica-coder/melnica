<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?= ClientRenderHelper::pageHeader(); ?></title>
    <base href="<?= ClientRenderHelper::pageBase(true); ?>/" />
<!--    <meta content='initial-scale=0.1;' name='viewport' /> -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="js/script-3.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <!-- Fonts -->
    <!--<link href='http://fonts.googleapis.com/css?family=Arimo&subset=cyrillic' rel='stylesheet' type='text/css'>-->
    <!--<link href='http://fonts.googleapis.com/css?family=PT+Serif&subset=cyrillic' rel='stylesheet' type='text/css'>-->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=API_KEY&sensor=false"></script>-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIS5K32_YsQOm9256AI53Fx39g2YHcU-s&sensor=false"></script>

    <link rel="stylesheet" type="text/css" href="css/MuseoSans-1.css" />
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="css/style-5.css?g=<?= rand(0, 1000) ?>" />
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="shortcut icon" href="images/favicon.ico"/>

</head>
<body>
<div id="header">
    <table class="clean navigation">
        <tr>
            <td class="column-left">
                    <?php
                        $actionId = Yii::app()->controller->action->id;
                        $isEventsAction = in_array($actionId, array('events', 'event', 'actionImportantEvents'));
                    ?>
                    <a href="<?= ClientRenderHelper::pageHref('events') ?>" class="soft-hover width-2 menu-item <?= $isEventsAction ? 'current' : '' ?>">
                        Новости
                    </a>
			 <a href="<?= ClientRenderHelper::pageHref('about') ?>" class="soft-hover width-2 menu-item <?= Yii::app()->controller->action->id == 'about' ? 'current' : '' ?>">
                                                о проекте                         </a>

					<a href="<?= ClientRenderHelper::pageHref('service') ?>" class="soft-hover menu-item <?= Yii::app()->controller->action->id == 'service' ? 'current' : '' ?>">
						Возможности
					</a>
					<!--<a href="<?= ClientRenderHelper::pageHref('planing') ?>" class="soft-hover menu-item <?= Yii::app()->controller->action->id == 'planing' ? 'current' : '' ?>">
						Планировки
					</a>-->
            </td>
            <td class="column-right">
			<!--		<a href="<?= ClientRenderHelper::pageHref('about') ?>" class="soft-hover width-2 menu-item <?= Yii::app()->controller->action->id == 'about' ? 'current' : '' ?>">
						О Проекте
					</a>-->
 					<a href="<?= ClientRenderHelper::pageHref('planing') ?>" style="padd" class="soft-hover menu-item <?= Yii::app()->controller->action->id == 'planing' ? 'current' : '' ?>">
                                            планировки
                                        </a>

					<a href="<?= ClientRenderHelper::pageHref('contacts') ?>" class="soft-hover width-2 menu-item <?= Yii::app()->controller->action->id == 'contacts' ? 'current' : '' ?>">
						Контакты
					</a>
                    <a target="_blank" href="<?= ClientRenderHelper::getContactByLabel('vk_group') ?>" class="soft-hover menu-item social vk">&nbsp;</a>
                    <a target="_blank" href="<?= ClientRenderHelper::getContactByLabel('ig_group') ?>" class="soft-hover menu-item social ig">&nbsp;</a>
                    <a target="_blank" href="<?= ClientRenderHelper::getContactByLabel('fb_group') ?>" class="soft-hover menu-item social fb">&nbsp;</a>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center">
                <a href="<?= ClientRenderHelper::pageHref('index') ?>" class="logo">&nbsp;</a>
            </td>
        </tr>
    </table>
</div>
<div id="content">
    <!-- Content -->
    <?= $content ?>
    <!-- Content -->
</div>
<div id="footer">
    <div class="links">
        <a href="<?= ClientRenderHelper::pageHref('events') ?>">новости</a>
        <a href="<?= ClientRenderHelper::pageHref('about') ?>">о проекте</a>
	<a href="<?= ClientRenderHelper::pageHref('service') ?>">возможности</a>
        <a href="<?= ClientRenderHelper::pageHref('planing') ?>">планировки</a>
        <a href="<?= ClientRenderHelper::pageHref('contacts') ?>">контакты</a>
    </div>

    <div class="social">
        <a href="<?= ClientRenderHelper::getContactByLabel('vk_group') ?>" target="_blank" class="vk"></a>
        <a href="<?= ClientRenderHelper::getContactByLabel('ig_group') ?>" target="_blank" class="ig"></a>
        <a href="<?= ClientRenderHelper::getContactByLabel('fb_group') ?>" target="_blank" class="fb"></a>
    </div>

    <div class="contacts">
        <div class="address">
            <?= ClientRenderHelper::getContactByLabel('full_address_1') ?>
        </div>
        <div class="phones">
            <span class="phone right-spacing"><?= ClientRenderHelper::getContactByLabel('city_phone_1') ?></span>
            <span class="phone"><?= ClientRenderHelper::getContactByLabel('city_phone_2') ?></span>
        </div>
    </div>
</div>

<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-48711104-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>

<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript">
try { var yaCounter24204853 = new Ya.Metrika({id:24204853,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true});
} catch(e) { }
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24204853" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</body>
</html>
