<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico"/>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
	<div id="header">
        <?php
            if (!Yii::app()->user->isGuest) {
                $items1 = array(
                    array(
                        'label' => 'Текстовые вставки',
                        'url' => Yii::app()->createUrl('texts/index')
                    ),
                    array(
                        'label' => 'Контакты',
                        'url' => Yii::app()->createUrl('contacts/index')
                    ),
                    array(
                        'label' => 'Файлы',
                        'url' => Yii::app()->createUrl('files/index')
                    ),
                    array(
                        'label' => 'События',
                        'url' => '#',
                        'items' => array(
                            array(
                                'label' => 'События',
                                'url' => Yii::app()->createUrl('events/index', array('type' => ImageGalleryType::TYPE_EVENTS))
                            ),
                            array(
                                'label' => 'Галереи событий',
                                'url' => Yii::app()->createUrl('imageGalleries/index', array('type' => ImageGalleryType::TYPE_EVENTS))
                            ),
                        )
                    ),
                    array(
                        'label' => 'Планировки',
                        'url' => '#',
                        'items' => array(
                            array(
                                'label' => 'Этажи',
                                'url' => Yii::app()->createUrl('floors/index')
                            ),
                            array(
                                'label' => 'Резиденты',
                                'url' => Yii::app()->createUrl('residents/index')
                            ),
                            array(
                                'label' => 'Галереи этажей',
                                'url' => Yii::app()->createUrl('imageGalleries/index', array('type' => ImageGalleryType::TYPE_FLOORS))
                            ),
                        )
                    ),
                );

                $items2 = array(
                    array(
                        'label' => 'Безопасность',
                        'url' => '#',
                        'items' => array(
                            array(
                                'label' => 'Администраторы',
                                'url' => Yii::app()->createUrl('admins/index'),
                            ),
                            array(
                                'label' => 'Разрешенные ip-адреса',
                                'url' => Yii::app()->createUrl('allowedIpAddresses/index'),
                            ),
                        )
                    ),
                    array(
                        'label' => 'Выход (' . Yii::app()->user->email . ')',
                        'url' => Yii::app()->createUrl('dashboard/logout'),
                    ),
                );

                $this->widget(
                    'TbNavbar',
                    array(
                        'brand' => Yii::app()->name,
                        'items' => array(
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'items' => $items1
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbMenu',
                                'htmlOptions' => array('class' => 'pull-right'),
                                'items' => $items2
                            )
                        )
                    )
                );
            }
        ?>
	</div>
    <br />
    <br />
    <br />
    <div id="content">
	    <?php echo $content; ?>
    </div>
</div>

</body>
</html>
