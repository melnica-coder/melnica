<div id="header">
    <div id="header-left" class="left">
        <div>
            <a href="<?= Yii::app()->createUrl('client/stylesMap'); ?>">Карта стилей</a>
        </div>
    </div>
    <div id="header-center" class="left">
        <div class="site-name">
            История Дизайна
        </div>
        <a href="<?= Yii::app()->createUrl('client/index'); ?>">
            <img src="images/logo-small.png" width="239" height="73" />
        </a>
        <div class="years">
            1819-2000
        </div>
        <?php
            $action = Yii::app()->getController()->getAction()->id;
        ?>
        <div class="items">
            <a href="<?= Yii::app()->createUrl('client/authors'); ?>" class="left <?= $action == 'author' || $action == 'authors' ? 'active' : ''; ?>">Авторы</a>
            <a href="<?= Yii::app()->createUrl('client/objects'); ?>" class="left <?= $action == 'object' || $action == 'objects' ? 'active' : ''; ?>">Объекты</a>
            <a href="<?= Yii::app()->createUrl('client/styles'); ?>" class="left <?= $action == 'style' || $action == 'styles' ? 'active' : ''; ?>">Стили</a>
            <br class="clear" />
        </div>
    </div>
    <div id="header-right" class="left">
        <div>
            <a href="<?= Yii::app()->createUrl('client/about'); ?>">О проекте</a>
        </div>
    </div>
    <br class="clear" />
</div>

<hr />