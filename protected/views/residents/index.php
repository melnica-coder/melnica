<div class="pull-right">
    <?php
        $this->widget(
            'TbButton',
            array(
                'label' => 'Добавить резидента',
                'type' => 'primary',
                'url' => $this->createUrl('residents/create')
            )
        );
    ?>
</div>

<h1>Резиденты</h1>

<p>
    <?php
        $this->widget(
            'TbGridView',
            array(
                'type' => 'striped bordered condensed',
                'dataProvider' => $provider,
                'template' => "{summary}\n{items}\n{pager}",
                'columns' => array(
                    array(
                        'name' => 'name',
                        'headerHtmlOptions' => array('class' => 'span3')
                    ),
                    array(
                        'class' => 'VMImagePreviewColumn',
                        'name' => 'image_filename',
                        'headerHtmlOptions' => array('class' => 'span8')
                    ),
                    array(
                        'header' => 'Действия',
                        'class' => 'TbButtonColumn',
                        'headerHtmlOptions' => array('class' => 'span1'),
                        'template' => '{update} {delete}',
                        'buttons' => array(
                            'update' => array(
                                'label' => 'Изменить',
                            ),
                            'delete' => array(
                                'label' => 'Удалить',
                            ),
                        ),
                    ),
                ),
            )
        );
    ?>

</p>