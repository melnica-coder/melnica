<h1>Error <?php echo $code; ?></h1>
<div class="error">
    <div>
        <h4><?php echo CHtml::encode($message); ?></h4>
    </div>
    <h3>Trace</h3>
    <div>
        <pre><?php echo CHtml::encode($trace); ?>
        </pre>
    </div>
</div>