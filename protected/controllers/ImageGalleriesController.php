<?php

class ImageGalleriesController extends AdminBackendController
{
    public function actionIndex()
    {
        $type = Yii::app()->request->getParam('type');
        if ($type == NULL) {
            throw new CException('Missing gallery type param');
        }

        $provider = new CActiveDataProvider('ImageGalleryExt');
        $provider->criteria->compare('type', $type);

        $this->render(
            'index',
            array(
                'provider' => $provider,
                'type' => $type
            )
        );
    }

    public function actionImages()
    {
        $imageGalleryId = Yii::app()->request->getParam('imageGalleryId');
        if ($imageGalleryId == NULL) {
            throw new CException('Missing gallery id param');
        }

        $imageGallery = ImageGalleryExt::model()->findByPk($imageGalleryId);
        if ($imageGallery == NULL) {
            throw new CException('Invalid image gallery');
        }

        $image = new ImageExt();
        $image->image_gallery_id = $imageGalleryId;

        $provider = new CActiveDataProvider('ImageExt');
        $provider->criteria->compare('image_gallery_id', $imageGalleryId);

        $this->render(
            'images',
            array(
                'model' => $image,
                'provider' => $provider,
                'imageGallery' => $imageGallery
            )
        );
    }

    public function actionCreate()
    {
        $type = Yii::app()->request->getParam('type');
        if ($type == NULL) {
            throw new CException('Missing gallery type param');
        }

        $imageGallery = new ImageGalleryExt();
        $imageGallery->type = $type;
        $this->render('edit', array('model' => $imageGallery));
    }

    public function actionUpdate()
    {
        $imageGallery = ImageGalleryExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $imageGallery));
    }

    public function actionDeleteImageGallery()
    {
        ImageGalleryExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionDeleteImage()
    {
        ImageExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('ImageGalleryExt');
        $saver->update();
    }

    public function actionAddImage()
    {
        $saver = new VMEntitySaver('ImageExt');
        $saver->safeSaving = true;

        $saver->afterSave = function(ImageExt $model) {
            VMUploadedMediaManager::quickSave($model, 'image');
        };

        $saver->beforeRenderIndex = function(VMEntitySaver $saver) {
            return array(
                'returnUrl' => Yii::app()->createUrl('imageGalleries/images', array('imageGalleryId' => $saver->object->image_gallery_id))
            );
        };

        $saver->save();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('ImageGalleryExt');
        $saver->safeSaving = true;

        $saver->beforeRenderIndex = function(VMEntitySaver $saver) {
            return array(
                'returnUrl' => Yii::app()->createUrl('imageGalleries/index', array('type' => $saver->object->type))
            );
        };

        $saver->save();
    }

    public function accessRules() {
        return array(
            // Deny access for non-authorized users
            array(
                'deny',
                'users' => array('?'),
            ),

            // Allow access for authorized users
            array(
                'allow',
                'users' => array('@'),
            ),
        );
    }
}