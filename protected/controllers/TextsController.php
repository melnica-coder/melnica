<?php

class TextsController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'comment ASC';

        $provider = new CActiveDataProvider(
            'TextExt',
            array(
                'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>50,
                ),
            )
        );
        $this->render('index', array('provider' => $provider));
    }

    public function actionCreate()
    {
        $texts = new TextExt();
        $this->render('edit', array('model' => $texts ));
    }

    public function actionUpdate()
    {
        $text  = TextExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render('edit', array('model' => $text));
    }

    public function actionDelete()
    {
        TextExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('TextExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('TextExt');
        $saver->safeSaving = true;

        $saver->save();
    }
}