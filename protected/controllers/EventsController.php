<?php

class EventsController extends AdminBackendController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'is_shown DESC, date ASC';

        $provider = new CActiveDataProvider('EventExt');
        $this->render(
            'index',
            array(
                'provider' => $provider,
                'criteria' => $criteria,
            )
        );
    }

    public function actionCreate()
    {
        $event = new EventExt();
        $this->render(
            'edit',
            array(
                'model' => $event,
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_EVENTS)), 'id', 'name')
            )
        );
    }

    public function actionUpdate()
    {
        $event = EventExt::model()->findByPk(Yii::app()->request->getParam('id'));
        $this->render(
            'edit',
            array(
                'model' => $event,
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_EVENTS)), 'id', 'name')
            )
        );
    }

    public function actionDelete()
    {
        EventExt::model()->deleteByPk(Yii::app()->request->getParam('id'));
    }

    public function actionEditable()
    {
        $saver = new TbEditableSaver('EventExt');
        $saver->update();
    }

    public function actionSave()
    {
        $saver = new VMEntitySaver('EventExt');
        $saver->safeSaving = true;

        $saver->beforeRenderUpdate = function(VMEntitySaver $saver) {
            return array(
                'imageGalleries' => CHtml::listData(ImageGalleryExt::model()->findAllByAttributes(array('type' => ImageGalleryType::TYPE_EVENTS)), 'id', 'name')
            );
        };

        $saver->afterSave = function(EventExt $model) {
            VMUploadedMediaManager::quickSave($model, 'banner');
            VMUploadedMediaManager::quickSave($model, 'icon');
        };

        $saver->save();
    }
}