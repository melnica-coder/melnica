<?php

class ClientController extends BaseBackendController
{
    public $layout = 'client';

    public function actionIndex()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница');
        $this->render(
            'index',
            array(
                'latestEvents' => EventExt::model()->getLatest()
            )
        );
    }

    public function actionAbout()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - О проекте');
        $this->render('about');
    }

    public function actionEvents()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - Новости');

        $type = Yii::app()->request->getParam('id');
        $isImportant = false;

        // Hack with filtering important events
        if ($type == EventType::TYPE_IMPORTANT) {
            $isImportant = true;
            $type = EventType::TYPE_ANY;
        }
        $modelEvent = new EventExt();
        $events = $modelEvent->getEventsByType($type, 16, 0, $isImportant);

        $offset = count($events);

        $this->render(
            'events',
            array(
                'events' => $events,
                'type' => $type,
                'offset' => $offset,
                'noMoreResults' => ($offset < 16) ? 1 : 0
            )
        );
    }

    public function actionEvent()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - Новости');

        $id = Yii::app()->request->getParam('id');
        $event = NULL;
        if ($id == NULL) {
            $this->redirect(Yii::app()->createUrl('client/index'));
        } else {
            $event = EventExt::model()->findByPk($id);
            if (!$event->is_shown) {
                $this->redirect(Yii::app()->createUrl('client/index'));
            }
        }

        if ($event == NULL) {
            throw new CException('Event id undefined or no events');
        }

        $this->render(
            'event',
            array(
                'event' => $event,
                'latestEvents' => EventExt::model()->getThreeLatest($id)
            )
        );
    }

    public function actionImportantEvents()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - Новости');

        $id = Yii::app()->request->getParam('id');
        $event = NULL;
        if ($id == NULL) {
            $event = EventExt::model()->findByAttributes(array('is_shown' => true));
        } else {
            $event = EventExt::model()->findByPk($id);
            if (!$event->is_shown) {
                $this->redirect(Yii::app()->createUrl('admins/index'));
            }
        }

        if ($event == NULL) {
            throw new CException('Event id undefined or no events');
        }

        $this->render(
            'importantEvents',
            array(
                'event' => $event,
                'latestEvents' => EventExt::model()->getThreeLatest($id, true)
            )
        );
    }

    public function actionService()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - Возможности');
        $this->render('service');
    }

    public function actionContacts()
    {
        Yii::app()->clientScript->reset();

        $this->setPageTitle('Мельница - Контакты');
        $this->render('contacts');
    }

    public function actionPlaning()
    {
        Yii::app()->clientScript->reset();
        $this->setPageTitle('Мельница - Планировки');

        $id = Yii::app()->request->getParam('id');
        $isDefaultPlaning = false;
        if ($id == NULL) {
            $this->redirect(ClientRenderHelper::pageHref('planing', array('id' => 1)));
        }

        $this->render(
            'planing',
            array(
                'floor' => FloorExt::model()->findByPk($id),
                'isDefaultPlaning' => $isDefaultPlaning
            )
        );
    }
}
