<?php

class AjaxController extends BaseBackendController
{
    public $layout = 'moreNews';

    public function actionMoreNews()
    {
        Yii::app()->clientScript->reset();

        $type = Yii::app()->request->getParam('id');
        $offset = Yii::app()->request->getParam('offset');

        $modelEvent = new EventExt();
        $events = $modelEvent->getEventsByType($type, 4, $offset);

        $this->renderPartial(
            'moreNews',
            array(
                'events' => $events
            ),
            false,
            true
        );
    }
}
